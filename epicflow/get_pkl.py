import glob, pickle, os
import numpy  as np

path = '/nfs/bigeye/yangwang/DataSets/TVHands/v2/clips/';
i = 0
for x in glob.glob(path+"*"):
    i += 1
    v = x.split("/")[-1]
    y = glob.glob(v+"/*")[0].split("/")[-1]
    for z in range(1,16):
        k = []
        k2 = []
        try:
            for q in open("{}/{}/frm_{:03d}_u.txt".format(v,y,z), "r").read().splitlines():
                k.append(list(map(float, q.split())))
            for q in open("{}/{}/frm_{:03d}_v.txt".format(v,y,z), "r").read().splitlines():
                k2.append(list(map(float, q.split())))
            pickle.dump((np.array(k), np.array(k2)), open('/nfs/bigeye/yangwang/DataSets/TVHands/v2/epicflows/{}|{}'.format(v, z), "wb"))
        except:
            print("error on {}|{}".format(v, z))
            x = open("errors3.txt", "a")
            x.write("{}|{}".format(v, z))
            x.close()
    if i % 10 == 0:
        print(i)
        
#pickle.dump(r, open(p, "wb"))

