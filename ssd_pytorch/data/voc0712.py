"""VOC Dataset Classes

Original author: Francisco Massa
https://github.com/fmassa/vision/blob/voc_dataset/torchvision/datasets/voc.py

Updated by: Ellis Brown, Max deGroot
"""

import os
import os.path
import sys
import torch
import torch.utils.data as data
import torchvision.transforms as transforms
import pickle
from PIL import Image, ImageDraw, ImageFont
import cv2
import numpy as np
import scipy.io as sio
import random
import opt_flow
if sys.version_info[0] == 2:
    import xml.etree.cElementTree as ET
else:
    import xml.etree.ElementTree as ET

VOC_CLASSES = (  # always index 0
    'aeroplane', 'bicycle', 'bird', 'boat',
    'bottle', 'bus', 'car', 'cat', 'chair',
    'cow', 'diningtable', 'dog', 'horse',
    'motorbike', 'person', 'pottedplant',
    'sheep', 'sofa', 'train', 'tvmonitor')

# for making bounding boxes pretty
COLORS = ((255, 0, 0, 128), (0, 255, 0, 128), (0, 0, 255, 128),
          (0, 255, 255, 128), (255, 0, 255, 128), (255, 255, 0, 128))


class AnnotationTransform(object):
    """Transforms a VOC annotation into a Tensor of bbox coords and label index
    Initilized with a dictionary lookup of classnames to indexes

    Arguments:
        class_to_ind (dict, optional): dictionary lookup of classnames -> indexes
            (default: alphabetic indexing of VOC's 20 classes)
        keep_difficult (bool, optional): keep difficult instances or not
            (default: False)
        height (int): height
        width (int): width
    """

    def __init__(self, class_to_ind=None, keep_difficult=False):
        self.class_to_ind = class_to_ind or dict(
            zip(VOC_CLASSES, range(len(VOC_CLASSES))))
        self.keep_difficult = keep_difficult

    def __call__(self, target, width, height):
        """
        Arguments:
            target (annotation) : the target annotation to be made usable
                will be an ET.Element
        Returns:
            a list containing lists of bounding boxes  [bbox coords, class name]
        """
        res = []
        for obj in target.iter('object'):
            difficult = int(obj.find('difficult').text) == 1
            if not self.keep_difficult and difficult:
                continue
            name = obj.find('name').text.lower().strip()
            bbox = obj.find('bndbox')

            pts = ['xmin', 'ymin', 'xmax', 'ymax']
            bndbox = []
            for i, pt in enumerate(pts):
                cur_pt = int(bbox.find(pt).text) - 1
                # scale height or width
                cur_pt = cur_pt / width if i % 2 == 0 else cur_pt / height
                bndbox.append(cur_pt)
            label_idx = self.class_to_ind[name]
            bndbox.append(label_idx)
            res += [bndbox]  # [xmin, ymin, xmax, ymax, label_ind]
            # img_id = target.find('filename').text[:-4]

        return res  # [[xmin, ymin, xmax, ymax, label_ind], ... ]

class TVHands(data.Dataset):
    def __init__(self, img_list, img_path, anno_path, flow_path, transform, add_channels, scale_flow=1, percent_ds=1, augment=8, gt_scale=1): # [8, 'M'] == 8-9 dx, 8-9 dy, magnitude
        n = []
        USE_FRAMES_UP_TO = max(augment, 8)
        print("Using frames up to {}".format(USE_FRAMES_UP_TO))
        print("GT Scale is {}".format(gt_scale))
        self.augment = augment
        for x in img_list:
            for j in range(8, USE_FRAMES_UP_TO+1):
                n.append((x, j))

        self.images = sorted(random.sample(n, int(len(img_list)*percent_ds)))
        self.img_path = img_path
        self.anno_path = anno_path
        self.transform = transform
        self.flow_path = flow_path
        self.rev = {}
        self.gt_scale = gt_scale
        self.scale_flow = scale_flow
        self.no_rgb = 'N' in add_channels
        self.channels = [x for x in add_channels if not (isinstance(x, str) and x[0] == 'N')]
        for i, path in enumerate(self.images):
            self.rev[path] = i
    def _bound(self, x):
        return min(1, max(0, x))
    def __getitem__(self, index): 
        old = False #self.augment <= 8
        s = self.images[index]
        frame_num = sorted(os.listdir(os.path.join(self.img_path, s[0])))[0]
        if old:
            annotation = sio.loadmat(os.path.join(self.anno_path, s[0], frame_num, "frm_008_hand.mat"))
        else:
            f = open(os.path.join(self.anno_path, "{}|{}".format(*s)))
        all_hands = []
        path = os.path.join(self.img_path, s[0], frame_num, "frm_{:03d}.png".format(s[1]))
        img = cv2.imread(path)
        if img is None or not hasattr(img, 'shape') or len(img.shape) < 3:
            print("ERROR READING", path)
            exit(1)

        channels = []
        for c in self.channels:
            if isinstance(c, str) and c[0] == 'M':
                channels.append(np.sqrt((channels[-1]**2)+(channels[-2]**2)).copy())
            elif isinstance(c, int):
                ok = False
                retried = False
                if not old:
                    c -= 1
                for i in range(2):
                    try:
                        x, y = pickle.load(open(os.path.join(self.flow_path, s[0]+"|"+str(c)), "rb"))
                        if len(x.shape) < 2 or len(y.shape) < 2:
                            raise Exception
                        ok = True
                        break
                    except Exception as e:
                        retried = True
                        print("ERROR IN LOADING FLOW")
                        print(str(e))
                        print("on image {}".format(s[0]+"|"+str(c)))
                        opt_flow.opt_flow_from_id(s[0], c)
                        print("Generated, retrying")
                if not ok:
                    print("ABORTING")
                    exit(1)
                elif retried:
                    print("Success!")
                channels += [x*self.scale_flow, y*self.scale_flow]
            else:
                print("Unrecognized channel setting {}".format(str(c)))
                exit(1)
        if len(channels):
            channels = np.asarray(channels).transpose(1,2,0)

        if old:
            for x in annotation['boxes']:
                l = [(int(x[0][0][0][v][0][0]), int(x[0][0][0][v][0][1])) for v in range(4)]
                xmin = xmax = l[0][0]
                ymin = ymax = l[0][1]
                for p in l:
                        xmin = min(xmin, p[0])
                        xmax = max(xmax, p[0])
                        ymin = min(ymin, p[1])
                        ymax = max(ymax, p[1])
                xmin /= img.shape[1]
                xmax /= img.shape[1]
                ymin /= img.shape[0]
                ymax /= img.shape[0]
                xmin = self._bound(xmin); xmax = self._bound(xmax)
                ymin = self._bound(ymin); ymax = self._bound(ymax)
                all_hands.append(np.asarray([xmin, ymin, xmax, ymax, 0], dtype=np.float32))
        else:
            for x in f.read().splitlines():
                l = list(map(lambda x: int(float(x)), x.split()))
                x_min = min(l[0::2])
                x_max = max(l[0::2])
                y_min = min(l[1::2])
                y_max = max(l[1::2])

                x_min /= img.shape[1]
                x_max /= img.shape[1]
                y_min /= img.shape[0]
                y_max /= img.shape[0]
                x_min = self._bound(x_min); x_max = self._bound(x_max)
                y_min = self._bound(y_min); y_max = self._bound(y_max)
                all_hands.append(np.asarray([x_min, y_min, x_max, y_max, 0], dtype=np.float32))
        target = np.asarray(all_hands, dtype=np.float32)

        if target.shape == (0,):
            x = [random.random() for _ in range(4)]
            target = np.asarray([np.asarray([min(x[0], x[1]), min(x[2], x[3]), max(x[0], x[1]), max(x[2], x[3]), -1], dtype=np.float32)], dtype=np.float32)

        if len(channels):
            img = np.append(img, channels, axis=2)

        img, boxes, labels = self.transform(img, target[:, :4], target[:, 4]) 
        t = (2, 1, 0)
        for i in range(img.shape[2]-3):
            t = t+(i+3,)
        img = img[:, :, t]
        target = np.hstack((boxes, np.expand_dims(labels, axis=1)))

        img = torch.from_numpy(img).permute(2, 0, 1)
        if self.no_rgb:
            img = img[3:, :, :]

        cx = np.mean(target[:, 0:4:2], axis=1)[..., np.newaxis]
        cy = np.mean(target[:, 1:4:2], axis=1)[..., np.newaxis]
        target[:, 0:4:2] = (target[:, 0:4:2]*self.gt_scale-cx*(self.gt_scale-1)).clip(0, 1)
        target[:, 1:4:2] = (target[:, 1:4:2]*self.gt_scale-cy*(self.gt_scale-1)).clip(0, 1)

        return (img, target)

    def __len__(self):
        return len(self.images)
    def pull_image(self, index):
        s = self.images[index]
        frame_num = sorted(os.listdir(os.path.join(self.img_path, s)))[0]
        path = os.path.join(self.img_path, s, frame_num, "frm_008.png")
        return cv2.imread(path, cv2.IMREAD_COLOR)
    def get_id(self, index):
        return self.images[index]


class VOCDetection(data.Dataset):
    """VOC Detection Dataset Object

    input is image, target is annotation

    Arguments:
        root (string): filepath to VOCdevkit folder.
        image_set (string): imageset to use (eg. 'train', 'val', 'test')
        transform (callable, optional): transformation to perform on the
            input image
        target_transform (callable, optional): transformation to perform on the
            target `annotation`
            (eg: take in caption string, return tensor of word indices)
        dataset_name (string, optional): which dataset to load
            (default: 'VOC2007')
    """

    def __init__(self, root, image_sets, transform=None, target_transform=None,
                 dataset_name='VOC0712'):
        self.root = root
        self.image_set = image_sets
        self.transform = transform
        self.target_transform = target_transform
        self.name = dataset_name
        self._annopath = os.path.join('%s', 'Annotations', '%s.xml')
        self._imgpath = os.path.join('%s', 'JPEGImages', '%s.jpg')
        self.ids = list()
        for (year, name) in image_sets:
            rootpath = os.path.join(self.root, 'VOC' + year)
            for line in open(os.path.join(rootpath, 'ImageSets', 'Main', name + '.txt')):
                self.ids.append((rootpath, line.strip()))

    def __getitem__(self, index):
        im, gt, h, w = self.pull_item(index)

        return im, gt

    def __len__(self):
        return len(self.ids)

    def pull_item(self, index):
        img_id = self.ids[index]

        target = ET.parse(self._annopath % img_id).getroot()
        img = cv2.imread(self._imgpath % img_id)
        height, width, channels = img.shape

        if self.target_transform is not None:
            target = self.target_transform(target, width, height)

        if self.transform is not None:
            target = np.array(target)
            img, boxes, labels = self.transform(img, target[:, :4], target[:, 4])
            # to rgb
            img = img[:, :, (2, 1, 0)]
            # img = img.transpose(2, 0, 1)
            target = np.hstack((boxes, np.expand_dims(labels, axis=1)))
        return torch.from_numpy(img).permute(2, 0, 1), target, height, width
        # return torch.from_numpy(img), target, height, width

    def pull_image(self, index):
        '''Returns the original image object at index in PIL form

        Note: not using self.__getitem__(), as any transformations passed in
        could mess up this functionality.

        Argument:
            index (int): index of img to show
        Return:
            PIL img
        '''
        img_id = self.ids[index]
        return cv2.imread(self._imgpath % img_id, cv2.IMREAD_COLOR)

    def pull_anno(self, index):
        '''Returns the original annotation of image at index

        Note: not using self.__getitem__(), as any transformations passed in
        could mess up this functionality.

        Argument:
            index (int): index of img to get annotation of
        Return:
            list:  [img_id, [(label, bbox coords),...]]
                eg: ('001718', [('dog', (96, 13, 438, 332))])
        '''
        img_id = self.ids[index]
        anno = ET.parse(self._annopath % img_id).getroot()
        gt = self.target_transform(anno, 1, 1)
        return img_id[1], gt

    def pull_tensor(self, index):
        '''Returns the original image at an index in tensor form

        Note: not using self.__getitem__(), as any transformations passed in
        could mess up this functionality.

        Argument:
            index (int): index of img to show
        Return:
            tensorized version of img, squeezed
        '''
        return torch.Tensor(self.pull_image(index)).unsqueeze_(0)


def detection_collate(batch):
    """Custom collate fn for dealing with batches of images that have a different
    number of associated object annotations (bounding boxes).

    Arguments:
        batch: (tuple) A tuple of tensor images and lists of annotations

    Return:
        A tuple containing:
            1) (tensor) batch of images stacked on their 0 dim
            2) (list of tensors) annotations for a given image are stacked on 0 dim
    """
    targets = []
    imgs = []
    for sample in batch:
        imgs.append(sample[0])
        targets.append(torch.FloatTensor(sample[1]))
    return torch.stack(imgs, 0), targets
