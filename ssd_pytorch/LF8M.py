scale_flow = 1 # 5
percent_training = 1 #.7
augment = 8 # 1-15, <=8 => no augment
channels = [8, 'M']
USE_LF = True
args.flow_weights = "weights/N8M/ssd300_0712_iter_100000.pth"
args.rgb_weights = "weights/ssd300_0712_iter_100000.pth"
