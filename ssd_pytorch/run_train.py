#!/usr/bin/env python
import os, time

#PATH = "weights/6M7M8M9M10M_LF_AUG15/ssd300_0712_iter_{}.pth"
PATH = "weights/8M_LF/ssd300_0712_iter_{}.pth"
OF = "LF8M.py"
#OF = "LF6M-10M_AUG.py"
#OF = "blank.py"
DEVICES = [3,1] # blank = all
#DEVICES = [0,2,3] # blank = all
BS = None # None = default

for i in range(200):
    j = 100000
    while not os.path.isfile(PATH.format(j)):
        j -= 2000
        if j == 0: break
    try:
        args = []
        if len(DEVICES):
            args += ["CUDA_VISIBLE_DEVICES="+",".join(map(str, DEVICES))]
        args += ["python", "train.py", "-of", OF]
        if j != 0:
            args += ["--resume={}".format(PATH.format(j)), "--start_iter={}".format(j)]
        if BS is not None:
            args += ["--batch_size={}".format(BS)]
        x = " ".join(args)
        print("----------- RUNNING ---------- ")
        print(x)
        print("--------")
        os.system(x)
    except Exception as e:
        print(str(e))
        pass
    print("Sleeping (10)")
    time.sleep(10)
    print("Continuing...")

