# /nfs/bigbrain/yangwang/Projects/Pytorch@UCF101/vgg16_rgb/main.py

from __future__ import print_function
import argparse
import os
import shutil
import time

import numpy as np
import scipy.io as sio
import h5py

import torch
import torch.cuda as cutorch
import torch.nn as nn
import torch.optim
import torchvision.models

parser = argparse.ArgumentParser(description='PyTorch Training')
# gpu
parser.add_argument('--gpuID',  default=-1, type=int, help='force to use gpuID')
parser.add_argument('--gpuMem', default=5,  type=float, help='minimum gpuMem (GB)')
# data
parser.add_argument('--trDir',  default='../input/rgb/s1/', help='train path')
parser.add_argument('--mInput', default='',                 help='input mean')
# model
parser.add_argument('--nClass',  default=101, type=int, help='number of classes')
parser.add_argument('--dropout', default=0.8, type=float, help='dropout ratio')
# loss
# optimization
parser.add_argument('--BtSz',  default=64,   type=int, help='batch size')
parser.add_argument('--sBtSz', default=16,   type=int, help='sub-batch size')
parser.add_argument('--LR',    default=1e-3, type=float, help='learning rate')
parser.add_argument('--MM',    default=0.9,  type=float, help='momentum')
parser.add_argument('--WD',    default=5e-4, type=float, help='weight decay')
# init & checkpoint
parser.add_argument('--initModel',  default='pretrained/vgg16_rgb_ucf101_s1/init.pth', help='init model')
parser.add_argument('--checkpoint', default=0,  type=int, help='resume from a checkpoint')
parser.add_argument('--nEpoch',     default=50, type=int, help='total epochs to run')
parser.add_argument('--saveDir',    default='ft/s1/', help='directory to save/log experiments')
parser.add_argument('--saveStep',   default=10, type=int,  help='epoch step for snapshot')
# misc

"""
def setGPU():
    # gpuID: no force -> first one with enough memory
    if opts.gpuID < 0:
        for i in range(cutorch.device_count()):
            if cutorch.getMemoryUsage(i) > (opts.GpuMem*1E9):
                opts.gpuID = i
                break

    if opts.gpuID < 0:
        print('no available GPU with > %d GB memory'%(opts.gpuMem))
        quit()
    else:
        print('using GPU %d'%(opts.gpuID));
        cutorch.device(opts.gpuID);
"""

def create_model():
    # vgg16
    model = torchvision.models.vgg16(num_classes=opts.nClass)
    # modify dropout ratio
    model.classifier[2].p = opts.dropout
    model.classifier[5].p = opts.dropout
    # initialize
    if opts.initModel=='' or opts.checkpoint >= 1:
        print('model initialized with gaussian noise')
    else:
        L = torch.load(opts.initModel);
        model.load_state_dict(L['state_dict'])
        print('model initialized with %s'%(opts.initModel));
    return model


def adjustLR(optimizer, epoch):
    # keep the same learning rate
    lr = opts.LR
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr

def mkdir_p(path):
    if not os.path.isdir(path):
        os.makedirs(path)

def logFile(epoch, subset):
    return '%s/%s_log/ep-%d.pt'%(opts.saveDir,subset, epoch)

def save_log(epoch, subset, state):
    log_file = logFile(epoch, subset)
    mkdir_p(os.path.dirname(log_file))
    torch.save(state, log_file)

def ckptFile(epoch):
    return '%s/ckpt/ep-%d.pt'%(opts.saveDir,epoch)

def resume_checkpoint(epoch, model, optimizer):
    ckpt_file = ckptFile(epoch)
    if os.path.isfile(ckpt_file):
        L = torch.load(ckpt_file)
        model.load_state_dict(L['state_dict'])
        optimizer.load_state_dict(L['optimizer'])
    else:
        print('checkpoint "%s" not found'%(ckpt_file))
        quit()

def save_checkpoint(epoch, model, optimizer):
    ckpt_file = ckptFile(epoch)
    state = {
        'epoch': epoch,
        'state_dict': model.state_dict(),
        'optimizer': optimizer.state_dict()
    }
    mkdir_p(os.path.dirname(ckpt_file))
    torch.save(state, ckpt_file)

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

def accuracy(output, target, topk=(1,)):
    """Computes the precision@k for the specified values of k"""

    maxk = max(topk)
    batch_size = target.size(0)

    _, pred = output.topk(maxk, 1, True, True)
    pred = pred.t()
    correct = pred.eq(target.view(1, -1).expand_as(pred))

    res = []
    for k in topk:
        correct_k = correct[:k].view(-1).float().sum(0)
        res.append(correct_k.mul_(1.0 / batch_size))
    return res


class TrLoader(object):

    def __init__(self):
        super(TrLoader, self).__init__()

    def getEpoch(self, epoch):
        trFile = '%s/tr%06d.mat'%(opts.trDir, epoch)
        f = h5py.File(trFile)
        self.data  = torch.ByteTensor(np.array(f['TrD'])).permute(3,2,1,0)
        self.label = torch.IntTensor(np.array(f['TrLb']).astype(int)).view(-1)
        self.size = self.label.size(0)

    def getBatch(self, inx):
        inx = inx.long();
        inputs = self.data.index_select(0,inx).float().cuda()
        inputs = inputs - 128   # mean-removal
        targets = self.label.index_select(0,inx).long().cuda()
        targets = targets - 1   # 0-based
        return (inputs, targets)


def train(trLD, model, criterion, optimizer, epoch):
    # switch to train mode (Dropout, BatchNorm, etc)
    model.train()

    # prepare training data
    print('loading training data for epoch %d'%(epoch))
    trLD.getEpoch(epoch)
    shuffle = torch.randperm(trLD.size)

    # forward and backward
    epLoss = AverageMeter()
    epTop1 = AverageMeter()
    for btStart in range(0, trLD.size//opts.BtSz*opts.BtSz, opts.BtSz): #remainder batch ignored 
        btEnd = min(btStart+opts.BtSz-1, trLD.size-1)
        btSz = btEnd-btStart+1
        btInx = shuffle.narrow(0, btStart, btSz)

        optimizer.zero_grad()
        for sbStart in range(0, btSz, opts.sBtSz):
            sbEnd = min(sbStart+opts.sBtSz-1, btSz-1)
            sbSz = sbEnd-sbStart+1
            sbInx = btInx.narrow(0, sbStart, sbSz)

            inputs, targets = trLD.getBatch(sbInx)
            input_var = torch.autograd.Variable(inputs)
            target_var = torch.autograd.Variable(targets)

            output = model(input_var)
            loss_sb = criterion(output, target_var)
            epLoss.update(loss_sb.data[0], sbSz)

            loss_bt = (loss_sb * sbSz) / btSz
            loss_bt.backward()

            (prec1,) = accuracy(output.data, targets, topk=(1,))
            epTop1.update(prec1[0], sbSz)

        # print('Epoch[%d], Batch[%d-%d], Loss[%.2f], Top1[%.2f]'
        #       %(epoch, btStart, btEnd, epLoss.avg, epTop1.avg))
        optimizer.step()

    # logging
    print('Epoch [%d], Loss[%.2f], Top1[%.2f]'%(epoch, epLoss.avg, epTop1.avg))
    state = {
        'epoch': epoch,
        'loss': epLoss.avg,
        'top1': epTop1.avg
    }
    save_log(epoch, 'train', state)

    # save checkpoint
    if epoch==1 or epoch%opts.saveStep==0:
        print('==> save model & optimizer @ epoch %d'%(epoch))
        save_checkpoint(epoch, model, optimizer)


def validate(valLoader, model, criterion, optimizer, epoch):
    pass

def main():

    print('==> parsing options')
    global opts
    opts = parser.parse_args()
    # setGPU()
    cutorch.device(0)
    # random seed

    print('==> create model, criterion, optimizer')
    model = create_model().cuda()
    criterion = nn.CrossEntropyLoss().cuda()
    optimizer = torch.optim.SGD(model.parameters(), opts.LR,
                                momentum=opts.MM,
                                weight_decay=opts.WD)    

    if opts.checkpoint >= 1:
        print('model resumed from epoch %d'%(opts.checkpoint))
        resume_checkpoint(opts.checkpoint, model, optimizer)
        start_epoch = opts.checkpoint + 1
    else:
        start_epoch = 1

    print('==> create data loader')
    trLD = TrLoader()

    print('==> start training from epoch %d'%(start_epoch))
    for epoch in range(start_epoch, opts.nEpoch+1):
        adjustLR(optimizer, epoch)
        train(trLD, model, criterion, optimizer, epoch)
        # validate(ValLoader(), model, criterion, epoch)

if __name__ == '__main__':
    main()
