import matplotlib.pyplot as plt
import numpy as np
import pickle
import random
import os, sys, glob

from my_utils import get_image
from visualize import draw_box
from aps import get_trues, get_trues_pseudo
from visualize import GetPreds

def run(img_id, frm):
    a,b = pickle.load(open("opt_flows/"+img_id+"|"+str(frm), "rb"))

    trues = get_trues_pseudo(img_id, frm)

    v = get_image(img_id, frm)
    f, g = plt.subplots(1, 3, figsize=(15, 5))
   # for t in trues:
   #     print(t)
   #     draw_box(v, t, (1, 0, 1), (0, 1, 1))
    g[0].imshow(v[..., ::-1])
    a = plt.get_cmap('Greys')(a)
   # for t in trues:
   #     draw_box(a, t, (1, 0, 1), (0, 1, 1))
    g[1].imshow(a)
    b = plt.get_cmap('Greys')(b)
   # for t in trues:
   #     draw_box(b, t, (1, 0, 1), (0, 1, 1))
    g[2].imshow(b)
    plt.show()

while True:
    if len(sys.argv) < 3:
        x = os.path.basename(random.choice(glob.glob("opt_flows/*")))
        img_id = x.split("|")[0]
        frm = int(x.split("|")[1])
        if frm < 8: continue
        run(img_id, frm)
    else:
        img_id = sys.argv[1]
        frm = int(sys.argv[2])
        if len(sys.argv) == 4:
            frm_end = int(sys.argv[3])
            for i in range(frm, frm_end+1):
                print("Frame {}".format(i))
                run(img_id, i)
        else:
            run(img_id, frm)
            
    
